@extends('blog::layouts.master')

@section('content')
    <div class="card mt-1">
        <div class="card-header">
            Add a post and participate!
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('blog.store') }}">
                @csrf
                <div class="form-group">
                    <label for="title">Post Title</label>
                    <input type="text" class="form-control" name="title" id="title">
                </div>

                <div class="form-group">
                    <label for="title">Post Category</label>
                    <select class="form-control" name="category_id">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="title">Post Body</label>
                    <textarea name="body" class="form-control"></textarea>
                </div>
                <input type="hidden" name="category_id" value="1">
                <button type="submit" class="btn btn-block btn-dark">Submit</button>
            </form>
        </div>
    </div>

    @foreach($posts as $post)
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ $post->url() }}">{{ $post->title }}</a>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ $post->excerpet }}.</p>
                        <a href="{{ $post->url() }}" class="btn btn-primary">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="row mt-5">
        <div class="col-md-12">
            {!! $posts->links() !!}
        </div>
    </div>
@stop
