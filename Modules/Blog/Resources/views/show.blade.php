@extends('blog::layouts.master')

@section('content')
    <div class="row mt-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ $post->url() }}">{{ $post->title }}</a>
                </div>
                <div class="card-body">
                    <p class="card-text">{{ $post->body }}.</p>
                </div>
            </div>
        </div>
    </div>
@stop
