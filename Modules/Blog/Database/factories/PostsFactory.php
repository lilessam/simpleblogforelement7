<?php

use Faker\Generator as Faker;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\Category;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(90),
        'body' => $faker->paragraph,
        'category_id' => function() {
            return factory(Category::class)->create()->id;
        }
    ];
});
