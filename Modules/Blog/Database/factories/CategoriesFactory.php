<?php

use Faker\Generator as Faker;
use Modules\Blog\Entities\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(20),
    ];
});
