<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\{Post, Category};

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $posts = Post::latest()->paginate(4);
        $categories = Category::latest()->get();
        return view('blog::index', [
            'posts'         => $posts,
            'categories'    => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $post = Post::create($request->all());
        return back();
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Post $post)
    {
        return view('blog::show', compact('post'));
    }
}
