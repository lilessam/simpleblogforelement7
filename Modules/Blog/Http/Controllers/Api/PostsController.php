<?php

namespace Modules\Blog\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return response()->json(Post::all()->toArray(), 200);
    }

    /**
     * Return a specific post as json.
     * @param int id
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($id)
    {
        return response()->json(Post::find($id)->toArray(), 200);
    }

    /**
     * Update a created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        Post::find($id)->update($request->all());
        return response()->json(['message' => 'Updated !'], 200);
    }
}
