<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Blog\Http\Controllers'], function()
{
    Route::get('/', 'BlogController@index')->name('blog.index');
    Route::get('/{post}', 'BlogController@show')->name('blog.show');
    Route::post('/', 'BlogController@store')->name('blog.store');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'api', 'namespace' => 'Modules\Blog\Http\Controllers\Api'], function()
{
    Route::get('/posts', 'PostsController@index');
    Route::get('/posts/{id}', 'PostsController@find');
    Route::post('/posts/{id}', 'PostsController@update');
    Route::get('/categories', 'CategoriesController@index');
});
