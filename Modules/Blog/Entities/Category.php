<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Blog\Entities\Post;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    /**
     * Posts of the current catgory.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id');
    }
}
