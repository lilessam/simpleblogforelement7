<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    use Sluggable;

    protected $guarded = [];

    protected $fillable = ['title', 'body', 'category_id'];

    protected $appends = ['excerpet'];

    /**
     * Append an excerpet attribute.
     * @return string
     */
    public function getExcerpetAttribute()
    {
        return mb_substr($this->body, 0, 200);
    }

    /**
     * Single post url.
     *
     * @return string
     */
    public function url()
    {
        return route('blog.show', ['post' => $this->slug]);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
