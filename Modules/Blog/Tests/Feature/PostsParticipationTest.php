<?php

namespace Modules\Blog\Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Blog\Entities\Post;

class PostsParticipationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_can_submit_a_post()
    {
        $post = factory(Post::class)->make();
        $this->post('/blog', $post->toArray())->assertStatus(302);
    }
}
