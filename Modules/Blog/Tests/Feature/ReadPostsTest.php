<?php

namespace Modules\Blog\Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Blog\Entities\Post;

class ReadPostsTest extends TestCase
{
    use RefreshDatabase;

    protected $post;

    public function setUp()
    {
        parent::setUp();
        $this->post = factory(Post::class)->create();
    }

    /** @test */
    public function a_guest_can_read_posts()
    {
        $this->get('/blog')->assertSee($this->post->title);
    }

    /** @test */
    public function a_guest_can_read_post()
    {
        $this->get($this->post->url())->assertSee($this->post->title);
    }
}
