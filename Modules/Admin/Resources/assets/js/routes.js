import AdminLogin from './pages/auth/AdminLogin';
import LoginLayout from './containers/LoginLayout'

import DashboardLayout from './containers/DashboardLayout';
import Posts from './pages/posts/List';
import PostsManager from './pages/posts/Manager';

var adminRoutes =  [
    {
        path: '/',
        name: 'Login',
        component: LoginLayout,
        children : [
            {
                path : '/',
                name : 'Admin Login',
                component : AdminLogin
            }
        ]
    },
    {
        path: '/panel',
        name: 'Panel',
        redirect: '/posts',
        meta: {
            auth: true
        }
    },
    {
        path: '/posts',
        name: 'posts',
        component: DashboardLayout,
        children : [
            {
                path : '/',
                name : 'PostsList',
                component : Posts
            }
        ],
        meta: {
            auth: true
        }
    },

    {
        path: '/posts/:id',
        name: 'PostsUpdate',
        component: DashboardLayout,
        children : [
            {
                path : '/',
                name : 'posts',
                component : PostsManager
            }
        ],
        meta: {
            auth: true
        }
    },
];

export default adminRoutes;
