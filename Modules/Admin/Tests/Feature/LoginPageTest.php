<?php

namespace Modules\Admin\Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginPageTest extends TestCase
{
    /** @test */
    public function login_page_can_be_found()
    {
        $this->get('/admin')->assertStatus(200);
    }

}
