<?php

use Faker\Generator as Faker;

$factory->define(Modules\Admin\Entities\User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstNameMale,
        'email' => 'erwinchafros@gmail.com',
        'is_admin' => 1,
        'password' => bcrypt(123456),
        'remember_token' => str_random(10),
    ];
});
