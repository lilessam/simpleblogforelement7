composer install

php artisan module:migrate Admin
php artisan module:migrate Blog
php artisan module:seed Admin
php artisan module:seed Blog

yarn
npm run dev

echo Installation is done but please remember to change the API endpoint in resources/assets/js/app.js if you didnt already
echo THANKS
