
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/** Import Vue.js **/
window.Vue = require('vue');

/** Import and use Vuetify **/
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
Vue.use(Vuetify);

/** Import and use VueX Store **/
import Vuex from 'vuex'
Vue.use(Vuex)

/** Import and use VueAxios **/
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

/** Setting the environment API URL **/
axios.defaults.baseURL = 'https://blog.test/api';

// Require the main app template which is empty
import App from './App.vue';

// Import the router
import router from './router';

Vue.router = router

/** Use Vue Auth Plugin **/
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
App.router = Vue.router

const app = new Vue({
    el: '#app',
    template: '<App/>',
    components: { App },
});

