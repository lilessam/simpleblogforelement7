import Vue from 'vue';
import Router from 'vue-router';
import adminRoutes from '../../../../Modules/Admin/Resources/assets/js/routes';

Vue.use(Router);

export default new Router({
    mode: 'hash', // hash or hash = Demo is living in GitHub.io, so required!
    linkActiveClass: 'open active',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        ...adminRoutes
    ]
});